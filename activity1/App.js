import React from 'react';
import {StyleSheet, SafeAreaView, TextInput, Button } from 'react-native';

export default class App extends React.Component {
  state= {TxtBox: ''
  }
  TxtBox = (text) => this.setState({TxtBox: text})
  render() {
return (
  <SafeAreaView style = {styles.container}>
    <TextInput style = {styles.in}
      textAlign = "center"
      placeholder = "Enter Your Text Here"
      placeholderTextColor = "#777B7E"
      onChangeText = {this.TxtBox}
      />
    <Button title = "Enter" onPress = {() => alert("You just typed: "+this.state.TxtBox)}
    color = "black"/>
  </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  in: {
    height: 40,
    width:300,
    margin: 30,
    borderWidth: 2.5
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#ffffff"
    }
});
